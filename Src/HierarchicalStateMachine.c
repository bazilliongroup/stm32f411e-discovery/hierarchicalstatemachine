/*
Library:			BasicStateMachine.c
Written by:			Ronald Bazillion
Date written:			07/02/2019
Description:			Implementation File for Basic State Machine
Resources:                      https://www.youtube.com/watch?v=pxaIyNbcPrA&index=9&list=PL5701E4325336503E&t=53s
                                https://www.youtube.com/playlist?list=PLn8PRpmsu08pHsOznk_WT6QT9cySw1A59
                                https://help.ubidots.com/en/articles/1161418-mqtt-finite-state-machines-and-ubidots-part-i
                                https://ubidots.com/blog/advantages_and_disadvantages_of_finite_state_machines/amp/?fbclid=IwAR13Hoih8N9JjepKhloYYnhRqDKZTXiNmeo3pCk-oGfVgSVG3g3hrMFKDdo
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "HierarchicalStateMachine.h"
#include "TimerModule.h"
#include "UserButton.h"
#include "Leds.h"
#include "main.h"
#include "UartProcessing.h"
#include <string.h> 

extern UART_COMMANDS heirarchicalUartValue;

//***** Defines ********//
//#define DEBUG

//***** typedefs enums *****//


//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//

static void m_actionM1State1Event1(void);
static void m_actionM1State1Event2(void);
static void m_actionM1State2Event1(void);
static void m_actionM1State2Event2(void);
static void m_actionM1State3Event1(void);
static void m_actionM1State3Event2(void);
static void m_actionM1State4Event1(void);
static void m_actionM1State4Event2(void);

static void m_actionM2State1Event1(void);
static void m_actionM2State1Event2(void);
static void m_actionM2State2Event1(void);
static void m_actionM2State2Event2(void);
static void m_actionM2State3Event1(void);
static void m_actionM2State3Event2(void);
static void m_actionM2State4Event1(void);
static void m_actionM2State4Event2(void);


//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//
static UART_HandleTypeDef* m_huart;

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

STATE_MACHINE_STRUCT SM_StateMachine;   //global variable used for State Machine; must be extern to use..

//table that processes events. the table is an array of function pointers.
FUNCT_PTR eventFctPointerTable1[MAX_STATES_MODE1][MAX_EVENTS_MODE1] =
{
  { m_actionM1State1Event1, m_actionM1State1Event2 }, /* procedures for state1 */
  { m_actionM1State2Event1, m_actionM1State2Event2 }, /* procedures for state2 */
  { m_actionM1State3Event1, m_actionM1State3Event2 }, /* procedures for state3 */
  { m_actionM1State4Event1, m_actionM1State4Event2 }  /* procedures for state4 */
};

FUNCT_PTR eventFctPointerTable2[MAX_STATES_MODE2][MAX_EVENTS_MODE2] =
{
  { m_actionM2State1Event1, m_actionM2State1Event2}, /* procedures for state1 */
  { m_actionM2State2Event1, m_actionM2State2Event2}, /* procedures for state2 */
  { m_actionM2State3Event1, m_actionM2State3Event2}, /* procedures for state3 */
  { m_actionM2State4Event1, m_actionM2State4Event2}  /* procedures for state4 */
};

void SM_Init(UART_HandleTypeDef* huart)
{  
  m_huart = huart;
  UserButtonInit();
  LedInit();
  LedRedTurnOn();
  
  const char* str = "Mode 1 Red State\r\n\n";
  size_t strSize = strlen(str);
  HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);  
  
  SM_StateMachine.States = RED_STATE_M1;
  SM_StateMachine.FuncPtr = SM_RedStateM1;
}

void SM_CommonTasks(void)
{
  UserButtonCheck();  
}

void SM_RedStateM1(void)
{ 
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable1[RED_STATE_M1][EVENT1_M1]();
    }
}

void SM_BlueStateM1(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable1[BLUE_STATE_M1][EVENT1_M1]();
    }  
}

void SM_GreenStateM1(void)
{  
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable1[GREEN_STATE_M1][EVENT1_M1]();
    }
    if (heirarchicalUartValue == UART_PARENT_TO_CHILD)
    {
        eventFctPointerTable1[GREEN_STATE_M1][EVENT2_M1]();
    }
}

void SM_OrangeStateM1(void)
{  
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable1[ORANGE_STATE_M1][EVENT1_M1]();
    }  
}

void SM_RedStateM2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable2[RED_STATE_M2][EVENT1_M2]();
    }  
}

void SM_BlueStateM2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable2[BLUE_STATE_M2][EVENT1_M2]();
    }    
}

void SM_GreenStateM2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable2[GREEN_STATE_M2][EVENT1_M2]();
    }  
}

void SM_OrangeStateM2(void)
{
    if (UserButtonGetPressed() && UserButtonGetDbn())
    {
        UserButtonSetPressed(false);
        UserButtonSetDbn(false);
        eventFctPointerTable2[ORANGE_STATE_M2][EVENT1_M2]();
    }    
    if (heirarchicalUartValue == UART_CHILD_TO_PARENT)
    {
        eventFctPointerTable2[ORANGE_STATE_M2][EVENT2_M2]();
    }
}

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************

//****************MODE 1************************************

static void m_actionM1State1Event1(void)  //red state event1 --> blue state
{
    const char* str = "Mode 1 Blue State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedRedTurnOff();
    LedBlueTurnOn();
    SM_StateMachine.States = BLUE_STATE_M1;
    SM_StateMachine.FuncPtr = SM_BlueStateM1;
}

static void m_actionM1State1Event2(void)  //red state event2 - no transitions for now
{
    heirarchicalUartValue = UART_INVALID;
}

static void m_actionM1State2Event1(void)  //blue state event1 --> green state
{
    const char* str = "Mode 1 Green State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedBlueTurnOff();
    LedGreenTurnOn();
    SM_StateMachine.States = GREEN_STATE_M1;
    SM_StateMachine.FuncPtr = SM_GreenStateM1;
}

static void m_actionM1State2Event2(void)  //blue state event2 - no transitions for now
{
    heirarchicalUartValue = UART_INVALID;
}

static void m_actionM1State3Event1(void)  //green state event1 --> orange state
{
    const char* str = "Mode 1 Orange State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedGreenTurnOff();
    LedOrangeTurnOn();
    SM_StateMachine.States = ORANGE_STATE_M1;
    SM_StateMachine.FuncPtr = SM_OrangeStateM1;    
}

static void m_actionM1State3Event2(void)  //green state event2 --> green state Mode 2
{
    const char* str = "Mode 2 Green State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedGreenTurnOff(); 
    TmrStart(LedGetGreenTimerId());
    SM_StateMachine.States = GREEN_STATE_M2;
    SM_StateMachine.FuncPtr = SM_GreenStateM2;  
}

static void m_actionM1State4Event1(void) //orange state event1 --> red state
{
    const char* str = "Mode 1 Red State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedOrangeTurnOff();
    LedRedTurnOn();
    SM_StateMachine.States = RED_STATE_M1;
    SM_StateMachine.FuncPtr = SM_RedStateM1;  
}

static void m_actionM1State4Event2(void)  //orange state event2 - no transitions for now
{  
    heirarchicalUartValue = UART_INVALID;
}

//****************MODE 2************************************

static void m_actionM2State1Event1(void) //red state event1 --> blue state
{
    const char* str = "Mode 2 Blue State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedRedTurnOff();
    LedBlueTurnOn();
    TmrStop(LedGetRedTimerId());
    TmrReset(LedGetRedTimerId());
    TmrStart(LedGetBlueTimerId());
    SM_StateMachine.States = BLUE_STATE_M2;
    SM_StateMachine.FuncPtr = SM_BlueStateM2;
}

static void m_actionM2State1Event2(void) //red state event1 --> No transition
{
}

static void m_actionM2State2Event1(void)  //blue state event1 --> green state
{
    const char* str = "Mode 2 Green State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedBlueTurnOff();
    LedGreenTurnOn();
    TmrStop(LedGetBlueTimerId());
    TmrReset(LedGetBlueTimerId());
    TmrStart(LedGetGreenTimerId());  
    SM_StateMachine.States = GREEN_STATE_M2;
    SM_StateMachine.FuncPtr = SM_GreenStateM2;  
}

static void m_actionM2State2Event2(void)  //blue state event1 --> No transition
{
}

static void m_actionM2State3Event1(void)  //green state event1 --> orange state
{
    const char* str = "Mode 2 Orange State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedGreenTurnOff();
    LedOrangeTurnOn();
    TmrStop(LedGetGreenTimerId());
    TmrReset(LedGetGreenTimerId());
    TmrStart(LedGetOrangeTimerId());
    SM_StateMachine.States = ORANGE_STATE_M2;
    SM_StateMachine.FuncPtr = SM_OrangeStateM2;    
}

static void m_actionM2State3Event2(void)  //green state event1 --> No transition
{
    heirarchicalUartValue = UART_INVALID;
}

static void m_actionM2State4Event1(void) //orange state event1 --> red state
{
    const char* str = "Mode 2 Red State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedOrangeTurnOff();
    LedRedTurnOn();
    TmrStop(LedGetOrangeTimerId());
    TmrReset(LedGetOrangeTimerId());
    TmrStart(LedGetRedTimerId());
    SM_StateMachine.States = RED_STATE_M2;
    SM_StateMachine.FuncPtr = SM_RedStateM2; 
}

static void m_actionM2State4Event2(void) //orange state event2 --> Transition to orange state Mode 1
{
    const char* str = "Mode 1 Orange State\r\n\n";
    size_t strSize = strlen(str);
    HAL_UART_Transmit(m_huart, (uint8_t *)str, strSize, 10);
    
    heirarchicalUartValue = UART_INVALID;
    LedOrangeTurnOff();
    TmrStop(LedGetOrangeTimerId());
    TmrReset(LedGetOrangeTimerId());
    LedOrangeTurnOn();
    SM_StateMachine.States = ORANGE_STATE_M1;
    SM_StateMachine.FuncPtr = SM_OrangeStateM1; 
}