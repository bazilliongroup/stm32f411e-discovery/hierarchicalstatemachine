/*
Library:			UartProcessing.c
Written by:			Ronald Bazillion
Date written:			07/17/2019
Description:			Uart functions and variables
Resources:                      
            
Modifications:
-----------------

*/

//***** Header files *****//
#include "UartProcessing.h"

//***** Defines ********//

//***** typedefs enums *****//

//***** typedefs structs *****//

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

UART_COMMANDS heirarchicalUartValue;

//***********************************************************************************
//***************************** STATIC FUNCTIONS ************************************
//***********************************************************************************