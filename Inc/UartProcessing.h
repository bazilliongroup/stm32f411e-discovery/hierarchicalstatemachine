/*
Library:			UartProcessing.h
Written by:			Ronald Bazillion
Date written:			07/03/2019
Description:			Uart objects and functions
Resources:                      
            
Modifications:
-----------------

*/

#ifndef __UART_PROCESSING_H_
#define __UART_PROCESSING_H_

//***** Header files *****//
#include "stm32f4xx_hal.h"

//***** Defines ********//
#define UART_TX_DATA_SIZE 50
#define UART_RX_DATA_SIZE 1

//*******Enums*****************//

//*******Flags****************//

//*****Structures *************//

typedef enum uart_commands
{
  UART_PARENT_TO_CHILD = 0xAA,  //170
  UART_CHILD_TO_PARENT = 0xAB,  //171
  UART_INVALID = 0xFF,
}UART_COMMANDS;

//***** Constant Variables *****//

//***** API Functions prototype *****//

#endif