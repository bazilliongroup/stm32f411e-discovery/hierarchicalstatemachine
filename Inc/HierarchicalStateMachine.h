/*
Library:			BasicStateMachine.h
Written by:			Ronald Bazillion
Date written:			07/02/2019
Description:			Header File for Basic State Machine
Resources:                      https://www.youtube.com/watch?v=pxaIyNbcPrA&index=9&list=PL5701E4325336503E&t=53s
                                https://www.youtube.com/playlist?list=PLn8PRpmsu08pHsOznk_WT6QT9cySw1A59
            
Modifications:
-----------------

*/

#ifndef __BASIC_STATE_MACHINE_H_
#define __BASIC_STATE_MACHINE_H_

//*******Include*****************//
#include "stm32f4xx_hal.h"

//*******Defines*****************//
#define MAX_STATES_MODE1 4
#define MAX_EVENTS_MODE1 2
#define MAX_STATES_MODE2 4
#define MAX_EVENTS_MODE2 2
//*******Enums*****************//

typedef enum
{
//********MODE 1***********  
  RED_STATE_M1     = 0,
  BLUE_STATE_M1    = 1, 
  GREEN_STATE_M1   = 2,
  ORANGE_STATE_M1  = 3,
//********MODE 2*********** 
  RED_STATE_M2     = 0,
  BLUE_STATE_M2    = 1, 
  GREEN_STATE_M2   = 2,
  ORANGE_STATE_M2  = 3,  
}BASIC_STATES;

typedef enum
{
//********MODE 1***********  
  EVENT1_M1 = 0,
  EVENT2_M1 = 1, 
//********MODE 2***********  
  EVENT1_M2 = 0,
  EVENT2_M2 = 1,
}BASIC_EVENTS;

//*******Flags****************//

//*****Structures *************//

typedef void (*FUNCT_PTR)(void);

typedef struct
{
  BASIC_STATES States;
  BASIC_EVENTS Events;
  FUNCT_PTR FuncPtr;            // My function Pointer to the state functions.        
}STATE_MACHINE_STRUCT;

//***** Constant Variables *****//

//***** API Functions prototype *****//

/* Name: SM_Init(void) 
   Description: Used to initialize the State Machine
   Inputs args: None
   Output args: None 
*/
void SM_Init(UART_HandleTypeDef* huart);

/* Name: SM_CommonTasks(void) 
   Description: entry POint for common functions that will be called regardless of any state.
   Inputs args: None
   Output args: None 
*/
void SM_CommonTasks(void);

//state machine functions
void SM_RedStateM1(void);
void SM_BlueStateM1(void);
void SM_GreenStateM1(void);
void SM_OrangeStateM1(void);
void SM_RedStateM2(void);
void SM_BlueStateM2(void);
void SM_GreenStateM2(void);
void SM_OrangeStateM2(void);

#endif