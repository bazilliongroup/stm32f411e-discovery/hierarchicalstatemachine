/*
Library:			Leds.h
Written by:			Ronald Bazillion
Date written:			07/13/2019
Description:			Leds header
Resources:                      
            
Modifications:
-----------------

*/

#ifndef __LEDS_H_
#define __LEDS_H_

//***** Header files *****//
#include "stm32f4xx_hal.h"

//***** Defines ********//

//*******Enums*****************//

//*******Flags****************//

//*****Structures *************//

//***** Constant Variables *****//

//***** API Functions prototype *****//

void LedInit(void);
void LedRedFunction(uint8_t var);
void LedBlueFunction(uint8_t var);
void LedGreenFunction(uint8_t var);
void LedOrangeFunction(uint8_t var);
void LedRedTurnOn();
void LedRedTurnOff();
void LedBlueTurnOn();
void LedBlueTurnOff();
void LedGreenTurnOn();
void LedGreenTurnOff();
void LedOrangeTurnOn();
void LedOrangeTurnOff();
uint8_t LedGetRedTimerId();
uint8_t LedGetBlueTimerId();
uint8_t LedGetGreenTimerId();
uint8_t LedGetOrangeTimerId();

#endif